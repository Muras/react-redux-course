import React from 'react';
import {Router, Route, IndexRoute, hashHistory } from 'react-router';

import App from './components/App';
import HomePage from './components/home/HomePage';
import AboutPage from './components/about/AboutPage';
import CoursesPage from './components/course/CoursesPage';

export default (
  <Router history={hashHistory}>
    <Route path='/' component={App}>
      <IndexRoute component={HomePage}/>
      <Route path='about' component={AboutPage}/>
      <Route path='courses' component={CoursesPage}/>      
    </Route>
  </Router>
);