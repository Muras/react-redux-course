import React from 'react';
import 'babel-polyfill'; //read more about this features!!!
import {render} from 'react-dom';
import routes from './routes';
import './styles/main.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

render (
  routes, 
  document.getElementById('app')
);